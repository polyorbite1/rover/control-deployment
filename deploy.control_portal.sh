# Build project
mkdir control_portal_temp
cd control_portal_temp

git clone https://gitlab.com/polyorbite1/rover/control-portal.git
mv control-portal/polyorbite-rover-control-portal/* ./
npm ci
npm run build

cd ..

# Create Docker image
sudo docker build -f control_portal.docker -t polyorbite_rover_control_portal ./control_portal_temp

# Remove project build
rm -R -f control_portal_temp